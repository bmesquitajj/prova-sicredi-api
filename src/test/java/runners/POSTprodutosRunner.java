package runners;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;

import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import dataFactory.produtosFactory;
import dto.hooks;
import dto.POSTprodutosDTO;

import static constants.produtosConstants.*;
import static io.restassured.RestAssured.given;

@Tags({@Tag("api"), @Tag("all")})
@TestMethodOrder(OrderAnnotation.class)
@Epic("SimuTest - API")
public class POSTprodutosRunner extends hooks {
	
	POSTprodutosDTO simuDTO = new POSTprodutosDTO();
	produtosFactory simuFactory = new produtosFactory();
	
	@Test
	@Order(1)
	@Description("Cenário: login de usuario")
		public void loginUserTest() {
			given().
				body(simuDTO.loginUser("loginUserSucesso")).
				contentType("application/json").
			when().
				post(ENDPOINTAUTHLOGIN).
			then().
				statusCode(HttpStatus.SC_OK).log().all();
		}
	
	@Test
	@Order(2)
	@Description("Cenário: login nome nulo")
		public void loginNameNull() {
			given().
				body(simuDTO.loginUser("loginUserNameNull")).
				contentType("application/json").
			when().
				post(ENDPOINTAUTHLOGIN).
			then().
				statusCode(HttpStatus.SC_BAD_REQUEST).log().all();
				Assert.assertEquals(simuFactory.getMensagem(), "Invalid credentials");
		}
	
	@Test
	@Order(3)
	@Description("Cenário: login senha nula")
		public void loginPassNull() {
			given().
				body(simuDTO.loginUser("loginUserPassNull")).
				contentType("application/json").
			when().
				post(ENDPOINTAUTHLOGIN).
			then().
				statusCode(HttpStatus.SC_BAD_REQUEST).log().all();
			Assert.assertEquals(simuFactory.getMensagem(), "Invalid credentials");
		}
	
	@Test
	@Order(4)
	@Description("Cenário: login de usuario inexistente")
		public void loginUserinexistente() {
			given().
				body(simuDTO.loginUser("loginUserInexistente")).
				contentType("application/json").
			when().
				post(ENDPOINTAUTHLOGIN).
			then().
				statusCode(HttpStatus.SC_BAD_REQUEST).log().all();
			Assert.assertEquals(simuFactory.getMensagem(), "Invalid credentials");
		}
	
	@Test
	@Order(5)
	@Description("Cenário: adicionando um produto")
		public void addProduto() {
			given().
				body(simuDTO.criarNovoProduto("novoCadastroSucesso")).
				contentType("application/json").
			when().
				post(ENDPOINTPRODUCTSADD).
			then().
				statusCode(HttpStatus.SC_OK).log().all();
		}
}
