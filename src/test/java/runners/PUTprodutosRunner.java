package runners;

import io.qameta.allure.Description;
import io.qameta.allure.Epic;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import static constants.produtosConstants.*;

import dataFactory.produtosFactory;
import dto.GETprodutosDTO;
import dto.PUTprodutosDTO;
import dto.hooks;
import static io.restassured.RestAssured.given;

@Tags({@Tag("api"), @Tag("all")})
@TestMethodOrder(OrderAnnotation.class)
@Epic("SimuTest - API")
public class PUTprodutosRunner extends hooks {
	
	GETprodutosDTO restDTO = new GETprodutosDTO();
	PUTprodutosDTO simuDTO = new PUTprodutosDTO();
	produtosFactory simuFactory = new produtosFactory();
	
	
	@Test
	@Order(1)
	@Description("Cenário: Alterando uma simulação")
		public void alterarSimulacaoSucesso() {
		given().
        	contentType("application/json").
        	pathParam("id", restDTO.novaPesquisaProduto("id-produto-padrao")).
        	body(simuDTO.alterarSimulacao("alteracaoSucesso")).
        when().
        	put(ENDPOINTPRODUCTSID).
        then().
        	statusCode(HttpStatus.SC_OK).log().all();
//			Assert.assertEquals(simuFactory.getBody().header("description"), "É perfume e não celular");
		
		}
	
	
	
}
