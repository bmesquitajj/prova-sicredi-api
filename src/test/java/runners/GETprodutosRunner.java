package runners;

import org.apache.http.HttpStatus;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import dataFactory.produtosFactory;
import dto.GETprodutosDTO;
import dto.hooks;

import static io.restassured.RestAssured.given;
import static constants.produtosConstants.*;

@Tags({@Tag("api"), @Tag("all")})
@Epic("RestricoesTest - API")
public class GETprodutosRunner extends hooks {
	
	GETprodutosDTO restDTO = new GETprodutosDTO();
	produtosFactory restFactory = new produtosFactory();
	
	@Test
	@Description("Cenário: Pesquisando produto test")
		public void comProdutosTest() {
		given().
       	contentType("application/json").
        when().
        	get(ENDPOINTTEST).
        then().
        	statusCode(HttpStatus.SC_OK).log().all();
		
		
		}
	
	@Test
	@Description("Cenário: Pesquisando produto GERAL")
		public void comProdutosGeral() {
			given().
	        	contentType("application/json").
	        when().
	        	get(ENDPOINTPRODUCTS).
	        then().
	        	statusCode(HttpStatus.SC_OK).log().all();
		
		}
	
	@Test
	@Description("Cenário: Pesquisando produto padrão id 1")
		public void produtoIdPadrao() {
			given().
	        	contentType("application/json").
	        	pathParam("id", restDTO.novaPesquisaProduto("id-produto-padrao")).
	        when().
	        	get(ENDPOINTPRODUCTSID).
	        then().
	        	statusCode(HttpStatus.SC_OK).log().all();
		
		
		}
	
	@Test
	@Description("Cenário: Pesquisando produto id inexistente")
		public void produtoInexistente() {
			given().
	        	contentType("application/json").
	        	pathParam("id", restDTO.novaPesquisaProduto("id-produto-inexistente")).
	        when().
	        	get(ENDPOINTPRODUCTSID).
	        then().
	        	statusCode(HttpStatus.SC_NOT_FOUND).log().all();
		
		}
	
	@Test
	@Description("Cenário: Pesquisando usuarios")
		public void pesquisandoUsuarios() {
		given().
        	contentType("application/json").
        when().
        	get(ENDPOINTUSERS).
        then().
        	statusCode(HttpStatus.SC_OK).log().all();
		
	}
	
	@Test
	@Description("Cenário: Pesquisando produto por usuario")
		public void pesquisandoProdutoUsuario() {
		given().
        	contentType("application/json").
        	header("Authorization", "Bearer " + restFactory.getToken()).
        when().
        	get(ENDPOINTAUTHPRODUCTS).
        then().
        	statusCode(HttpStatus.SC_OK).log().all();
		
		}
}
