package constants;

public class produtosConstants {
	
	public static final String ENDPOINTTEST = "test";
	public static final String ENDPOINTUSERS = "users";
	public static final String ENDPOINTAUTHLOGIN = "auth/login";
	public static final String ENDPOINTAUTHPRODUCTS = "auth/products";
	public static final String ENDPOINTPRODUCTS = "products";
	public static final String ENDPOINTPRODUCTSADD = "products/add";
	public static final String ENDPOINTPRODUCTSID = "products/{id}";
	
}
