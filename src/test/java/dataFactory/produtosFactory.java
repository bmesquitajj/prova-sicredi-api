package dataFactory;

import static io.restassured.RestAssured.given;
import static constants.produtosConstants.*;
import io.restassured.response.*;
import dto.GETprodutosDTO;
import dto.POSTprodutosDTO;


public class produtosFactory {
	POSTprodutosDTO simuDTO = new POSTprodutosDTO();
	GETprodutosDTO restDTO = new GETprodutosDTO();
	
	public String getId() {
        return given().
        		get(ENDPOINTTEST).
        		then().
        		extract().
        			path("");
    }
	
	public String getToken() {
        return given().
				body(simuDTO.loginUser("loginUserSucesso")).
				contentType("application/json").
			when().
					post(ENDPOINTAUTHLOGIN).
			then().
        		extract().
        			path("token");
    }
	
	public String getMensagem() {
        return given().
				body(simuDTO.loginUser("loginUserNameNull")).
				contentType("application/json").
			when().
					post(ENDPOINTAUTHLOGIN).
			then().
        		extract().
        			path("message");
    }
	
	
	public Response getBody() {
		return given().
		        	contentType("application/json").
		        	pathParam("id", restDTO.novaPesquisaProduto("id-produto-padrao")).
		        when().
		        	get(ENDPOINTPRODUCTSID).
				then().log().all().
				extract().response();

    }
	
}
