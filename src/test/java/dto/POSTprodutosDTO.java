package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class POSTprodutosDTO {
	
//	  "title": "Perfume Oil",
//    "description": "Mega Discount, Impression of A...",
//    "price": 13,
//    "discountPercentage": 8.4,
//    "rating": 4.26,
//    "stock": 65,
//    "brand": "Impression of Acqua Di Gio",
//    "category": "fragrances",
//    "thumbnail": "https://i.dummyjson.com/data/products/11/thumnail.jpg"
//	  "username": "kminchelle",
//    "password": "0lelplR"

	private String username;
	private String password;
	
	private String title;
	private String description;
	private String brand;
	private String category;
	private String thumbnail;
	private int price;
	private int stock;
	private double rating;
	private double discountPercentage;
	
	public POSTprodutosDTO criarNovoProduto(String type) {
		switch (type) {
			case "novoCadastroSucesso":
				 this.title = "Perfume Oil";
				 this.description = "Mega Discount, Impression of A...";
				 this.brand = "Impression of Acqua Di Gio";
				 this.category = "fragrances";
				 this.thumbnail = "https://i.dummyjson.com/data/products/11/thumnail.jpg";
				 this.price = 13;
				 this.stock = 65;
				 this.rating = 4.26;
				 this.discountPercentage = 8.4;
			break;

		}
		
		 return POSTprodutosDTO.builder().
				 title(this.getTitle()).
				 description(this.getDescription()).
				 brand(this.getBrand()).
				 category(this.getCategory()).
				 thumbnail(this.getThumbnail()).
				 price(this.getPrice()).
				 stock(this.getStock()).
				 rating(this.getRating()).
				 discountPercentage(this.getDiscountPercentage()).build();
	}
	
	public POSTprodutosDTO loginUser(String type) {
		switch (type) {
			case "loginUserSucesso":
				 this.username = "kminchelle";
				 this.password = "0lelplR";
				 
			break;
			
			case "loginUserNameNull":
				 this.username = "";
				 this.password = "0lelplR";
				 
			break;
			
			case "loginUserPassNull":
				 this.username = "kminchelle";
				 this.password = "";
				 
			break;
			
			case "loginUserInexistente":
				 this.username = "Teste";
				 this.password = "teste";
				 
			break;

		}
		
		 return POSTprodutosDTO.builder().
				 username(this.getUsername()).
				 password(this.getPassword()).build();
	}
}
