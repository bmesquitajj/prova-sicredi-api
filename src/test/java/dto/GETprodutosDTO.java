package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GETprodutosDTO {
	private int id;
	
	public int novaPesquisaProduto(String Type) {
		switch (Type) {
			case "id-produto-padrao":
				this.id = 1;
			break;
			
			case "id-produto-inexistente":
				this.id = 200;
			break;
			
		}
		
		return getId();
	}
}
