package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PUTprodutosDTO {
	private String username;
	private String password;
	
	private String title;
	private String description;
	private String brand;
	private String category;
	private String thumbnail;
	private int price;
	private int stock;
	private double rating;
	private double discountPercentage;
	
	public POSTprodutosDTO alterarSimulacao(String type) {
		switch (type) {
			case "alteracaoSucesso":
				 this.description = "É perfume e não celular";
				 this.brand = "Impression of Acqua Di Gio";
				 this.category = "fragrances";
				 this.thumbnail = "https://i.dummyjson.com/data/products/11/thumnail.jpg";
				 this.price = 95;
				 this.stock = 65;
				 this.rating = 4.26;
				 this.discountPercentage = 8.4;
			break;
		
		}
		
		 return POSTprodutosDTO.builder().
				 title(this.getTitle()).
				 description(this.getDescription()).
				 brand(this.getBrand()).
				 category(this.getCategory()).
				 thumbnail(this.getThumbnail()).
				 price(this.getPrice()).
				 stock(this.getStock()).
				 rating(this.getRating()).
				 discountPercentage(this.getDiscountPercentage()).build();
	}
}
