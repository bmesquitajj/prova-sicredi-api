# prova-sicredi-api



## Configurações iniciais

    Dependências: rest-assured 4.0.0, junit 4.12
    A estrutura do projeto contém uma interface chamada "Constants" onde as contantes do projeto estão armazenadas, na "produtosConstants.java" e na "hooks.java".
    Os testes estão divididos com GET, POST E PUT
    Clonar o repositório através do link: https://gitlab.com/bmesquitajj/prova-sicredi-api.git
    Instalar o jdk-21
    Instalar o apache maven 3.9.6
    IDE utilizada: Eclipse IDE
    Instalar a biblioteca Lombock https://dicasdeprogramacao.com.br/como-configurar-o-lombok-no-eclipse/
    * O lombockBuilder fora utilizado para criar maior dinamismo nos cenários de teste

    Instalar o relatório Allure Reports https://robsonagapito.medium.com/instalando-o-allure-no-windows-10-6c8440cd00e9

## Cenários de teste API:

- [ ] Tipo GET

```
    ("Cenário: Pesquisando produto test")
	("Cenário: Pesquisando produto GERAL")
	("Cenário: Pesquisando produto padrão id 1")
	("Cenário: Pesquisando produto id inexistente")
	("Cenário: Pesquisando usuarios")
	("Cenário: Pesquisando produto por usuario")
	
```
- [ ] Tipo POST

```
    ("Cenário: login de usuario")
	("Cenário: login nome nulo")
	("Cenário: login senha nula")
	("Cenário: login de usuario inexistente")
	("Cenário: adicionando um produto")
	
```
- [ ] Tipo PUT

```
    ("Cenário: Alterando uma simulação")
	
```
## Pontos de melhoria

- [ ] Existe a possibilidade de criar mais cenários de teste que envolvem PUT e DELETE
- [ ] Vincular allure reports ao git para relatórios após execução de pipeline [aqui](https://rapesil.medium.com/gerando-allure-report-de-forma-autom%C3%A1tica-com-github-actions-a514a07e146d)

## Relatório de teste

- [ ] Allure Reports
